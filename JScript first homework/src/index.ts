import express from "express";
import Server from "./server/Server";

const app = express();
const port = 8080;

new Server(app, port);
